package com.jmunoz.poc_mxc_kotlin.View.Fragments

import android.app.Fragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jmunoz.poc_mxc_kotlin.Model.LoginModelImpl
import com.jmunoz.poc_mxc_kotlin.Presenter.LoginPresenter
import com.jmunoz.poc_mxc_kotlin.R
import com.jmunoz.poc_mxc_kotlin.View.LoginView
import com.jmunoz.poc_mxc_kotlin.View.Utils.BusProvider

/**
 * Created by jmunoz on 03-07-17.
 */
class MainActivityFragment : Fragment() {
    var rootView: View? = null
    var presenter: LoginPresenter? = null
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        rootView = inflater?.inflate(R.layout.fragment_main, container, false)
        presenter = LoginPresenter(LoginModelImpl(context), LoginView(this, rootView, context, BusProvider().getInstance()))
        return rootView
    }

    override fun onResume() {
        super.onResume()
        BusProvider().register(presenter)
    }

    override fun onPause() {
        super.onPause()
        BusProvider().unregister(presenter)
    }
}