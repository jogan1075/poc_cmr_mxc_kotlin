package com.jmunoz.poc_mxc_kotlin.View.Entity

/**
 * Created by jogan1075 on 04-07-17.
 */
data class LoginData(var usuario: String, var password: String)