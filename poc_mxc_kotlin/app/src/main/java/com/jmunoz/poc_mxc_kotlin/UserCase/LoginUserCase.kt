package com.jmunoz.poc_mxc_kotlin.UserCase

import android.content.Context
import com.jmunoz.poc_mxc_kotlin.DataSources.DataSource
import com.jmunoz.poc_mxc_kotlin.Model.Callback
import com.jmunoz.poc_mxc_kotlin.Repository.RepositoryImpl

/**
 * Created by jogan1075 on 04-07-17.
 */
class LoginUserCase : UseCase {

    var repository: RepositoryImpl? = null

    constructor(context: Context?, callbacks: Callback?) : super() {
//        var DATA_SOURCE_1: DataSource? = null
        this.repository = RepositoryImpl(1, context, callbacks)
    }
    override fun execute(user: String, pass: String) {
        this.repository?.autentificacion(user, pass)
    }
}