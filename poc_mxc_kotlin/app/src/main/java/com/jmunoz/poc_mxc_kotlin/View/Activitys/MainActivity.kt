package com.jmunoz.poc_mxc_kotlin.View.Activitys

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.jmunoz.poc_mxc_kotlin.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
