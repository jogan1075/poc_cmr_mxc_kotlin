package com.jmunoz.poc_mxc_kotlin.DataSources.Factory

import android.content.Context
import com.jmunoz.poc_mxc_kotlin.BS.BSSecurityDSAuth
import com.jmunoz.poc_mxc_kotlin.DataSources.SecurityDSAuth
import com.jmunoz.poc_mxc_kotlin.Model.Callback

/**
 * Created by jogan1075 on 04-07-17.
 */
class DataSourceFactory {


     var dataSource: Int = 0
     var context: Context?=null
     var callback: Callback?=null

    constructor(dataSource: Int?, ctx: Context?, callback: Callback?){
        this.dataSource = dataSource as Int
        this.context = ctx
        this.callback = callback
    }

    fun createDataSource(): SecurityDSAuth? {
        when (this.dataSource) {
            1 -> return BSSecurityDSAuth(context, callback)
        }
        return null
    }
}