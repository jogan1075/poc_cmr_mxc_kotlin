package com.jmunoz.poc_mxc_kotlin.View.Utils

import android.os.Handler
import android.os.Looper
import com.squareup.otto.Bus
import com.squareup.otto.ThreadEnforcer

/**
 * Created by jogan1075 on 04-07-17.
 */
object EventBus : Bus(){

//    private val handler = Handler(Looper.getMainLooper())
//
//    fun MainPostingBus(){
////        super(ThreadEnforcer.ANY)
//    }
//
//    override fun post(event: Any) {
//        if (Looper.myLooper() == Looper.getMainLooper()) {
//            super.post(event)
//            return
//        }
//
//        handler.post { super@MainPostingBus.post(event) }
//    }

    private val _mainThreadHandler: Handler by lazy {
        Handler(Looper.getMainLooper())
    }

    override fun post(event: Any?) {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            super.post(event)
        } else {
            _mainThreadHandler.post {
                super.post(event)
            }
        }
    }
}