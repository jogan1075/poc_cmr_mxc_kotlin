package com.jmunoz.poc_mxc_kotlin.BS.Utils

/**
 * Created by jogan1075 on 04-07-17.
 */
enum class HttVerb(s: String) {

    POST("POST"),
    GET("GET"),
    DELETE("DELETE"),
    PUT("PUT"), ;

//     override var name: String? = null
//
//    private fun HttpVerb(name: String){
//        this.name = name
//    }

    fun getName(): String {
        return this.name
    }
}