package com.jmunoz.poc_mxc_kotlin.Model

/**
 * Created by jogan1075 on 04-07-17.
 */
interface Callback {

    fun successCall(message: String)
    fun erroCall(message: String?)
}