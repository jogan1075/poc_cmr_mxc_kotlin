package com.jmunoz.poc_mxc_kotlin.Presenter

import com.jmunoz.poc_mxc_kotlin.Model.Callback
import com.jmunoz.poc_mxc_kotlin.Model.LoginModelImpl
import com.jmunoz.poc_mxc_kotlin.View.LoginView
import com.squareup.otto.Subscribe

/**
 * Created by jmunoz on 04-07-17.
 */
class LoginPresenter : Callback {


    var loginModel: LoginModelImpl? = null
    var loginView: LoginView? = null

    constructor(loginModel: LoginModelImpl?, loginView: LoginView?) {
        this.loginModel = loginModel
        this.loginView = loginView
    }

    @Subscribe
    fun GetActionButton(event: LoginView.OnActivityEvent) {
        loginModel?.LoginUsuario(event.user, event.pass, this@LoginPresenter)


    }

    override fun successCall(message: String) {

        loginView?.didResponse(message)
    }

    override fun erroCall(message: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}