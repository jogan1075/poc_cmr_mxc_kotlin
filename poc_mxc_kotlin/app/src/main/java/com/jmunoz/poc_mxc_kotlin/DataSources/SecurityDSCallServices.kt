package com.jmunoz.poc_mxc_kotlin.DataSources

/**
 * Created by jogan1075 on 04-07-17.
 */
interface SecurityDSCallServices : DataSource {
    fun autentificacion(token: String)
}