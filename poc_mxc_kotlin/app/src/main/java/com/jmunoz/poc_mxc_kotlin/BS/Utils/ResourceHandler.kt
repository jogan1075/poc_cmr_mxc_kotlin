package com.jmunoz.poc_mxc_kotlin.BS.Utils

import android.app.ProgressDialog
import android.content.Context
import com.android.volley.Request
import com.jmunoz.poc_mxc_kotlin.Model.Callback

//import com.android.volley.re

/**
 * Created by jogan1075 on 04-07-17.
 */
abstract class ResourceHandler {
    var priorityRequest = Request.Priority.NORMAL
    private val pDialog: ProgressDialog
    val vollyRequest: VollyRequest
    var tokenAccess: String? = null
    open var call: Callback? = null

    constructor(callBack: Callback?, context: Context?) {
        this.pDialog = ProgressDialog(context)
        this.vollyRequest = VollyRequest(context).getInstance(context)
        this.call = callBack
    }

    fun getCallBack(): Callback? {
        return this.call
    }
    abstract fun getParamaters(json: String): String

    abstract val serviceURL: String

    abstract val httpVerb: HttVerb

//     fun getPriorityRequest(): Request.Priority {
//        return this.priorityRequest
//    }

    fun getpDialog(): ProgressDialog {
        pDialog.setMessage("Cargando...")
        return this.pDialog
    }

    protected val requestMethod: Int
        get() {
            when (httpVerb.getName()) {
                "POST" -> return Request.Method.POST
                "PUT" -> return Request.Method.PUT
                "DELETE" -> return Request.Method.DELETE
                "GET" -> return Request.Method.GET
                else -> return Request.Method.GET
            }
        }

    companion object {
         val TIMEOUT_MILLISECONDS = 60000
    }
}
