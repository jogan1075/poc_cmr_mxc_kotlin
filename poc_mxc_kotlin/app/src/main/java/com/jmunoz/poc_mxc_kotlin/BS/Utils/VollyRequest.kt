package com.jmunoz.poc_mxc_kotlin.BS.Utils

import android.content.Context
import com.android.volley.RequestQueue
import com.android.volley.toolbox.BasicNetwork
import com.android.volley.toolbox.DiskBasedCache
import com.android.volley.toolbox.HurlStack

/**
 * Created by jmunoz on 04-07-17.
 */
class VollyRequest {
    var context:Context?=null
    var mInstance: VollyRequest?=null
    var mRequestQueue: RequestQueue?=null

    constructor(context: Context?) {
        this.context = context
        mRequestQueue = getRequestQueue()
    }

    @Synchronized fun getInstance(context: Context?): VollyRequest {
        if (mInstance == null) {
            mInstance = VollyRequest(context)
        }
        return mInstance as VollyRequest
    }

    fun getRequestQueue(): RequestQueue {
        if (mRequestQueue == null) {
            val cache = DiskBasedCache(context?.getCacheDir(), 10 * 1024 * 1024)
            val network = BasicNetwork(HurlStack())
            mRequestQueue = RequestQueue(cache, network)
            // Don't forget to start the volley request queue
            (mRequestQueue as RequestQueue).start()
        }
        return mRequestQueue as RequestQueue
    }
}