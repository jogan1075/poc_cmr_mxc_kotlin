package com.jmunoz.poc_mxc_kotlin.Repository

import android.content.Context
import com.jmunoz.poc_mxc_kotlin.DataSources.Factory.DataSourceFactory
import com.jmunoz.poc_mxc_kotlin.DataSources.SecurityDSAuth
import com.jmunoz.poc_mxc_kotlin.Model.Callback

/**
 * Created by jogan1075 on 04-07-17.
 */
class RepositoryImpl : Repository {

    var dataSourceAuth: SecurityDSAuth? = null

    constructor(dataSource: Int?, context: Context?, callbacks: Callback?) {
        var ds= DataSourceFactory(dataSource,context,callbacks)
        this.dataSourceAuth = ds.createDataSource()
    }


    override fun autentificacion(user: String, pass: String) {
        dataSourceAuth?.autentificacion(user, pass)
    }


}