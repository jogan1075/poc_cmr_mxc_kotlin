package com.jmunoz.poc_mxc_kotlin.BS.Utils

import android.util.Base64
import java.security.KeyFactory
import java.security.NoSuchAlgorithmException
import java.security.PublicKey
import java.security.spec.InvalidKeySpecException
import java.security.spec.X509EncodedKeySpec
import javax.crypto.Cipher

/**
 * Created by jmunoz on 04-07-17.
 */
class RSAUtilSingleton {


    private val rsaPublicKeyString = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA4b2L3Gk8Q+qlcFQSRmho/fDPvrZ3A5jx3/9XE2X7N6EO2F9RcH23b8EY8hEEd7lHNXqHDYcpt97QPgey1lTqNItUrRqYFACU+5FFxUD6hwcibZFRaBy8PjHsfT2m9VcTlCAvViybHrwWMH73o9V8zLJfoFtxKKxfiTv4CBVIxLN+dTOJbqKgOYSRCG/3022rWcB4KpUqQlqTfp+3CreIIOcTVq9MZ41MvniMczBdC4ywOtBm/sd9acpUch2MKUbEe8nL9izfjahS6MADFAHqDTStCAsfPwh5S3yOj0cVtun6rkMhPG1Av6HT1bAWW8Du6BvF1ZKdC4ZY4t3/e005oQIDAQAB"

    private var rsaPublicKey: PublicKey? = null
    private var cipher: Cipher? = null

    fun RSAUtilSingleton() {}

    fun loadPublicKey() {

        val publicBytes = Base64.decode(rsaPublicKeyString, Base64.NO_WRAP)
        val keySpec = X509EncodedKeySpec(publicBytes)
        val keyFactory: KeyFactory
        try {
            keyFactory = KeyFactory.getInstance("RSA")
            rsaPublicKey = keyFactory.generatePublic(keySpec)
            cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding")
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        } catch (e: InvalidKeySpecException) {
            e.printStackTrace()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    fun encryptValue(textToEncrypt: String): String {
        var textEncrypted = ""

        try {
            cipher!!.init(Cipher.ENCRYPT_MODE, rsaPublicKey)
            val bit = cipher!!.doFinal(textToEncrypt.toByteArray(charset("UTF-8")))
            textEncrypted = Base64.encodeToString(bit, Base64.NO_WRAP)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return textEncrypted
    }

    fun decryptValue(textToDecrypt: String): String {
        var textDecrypted = ""
        try {
            cipher!!.init(Cipher.DECRYPT_MODE, rsaPublicKey)
            textDecrypted = String(cipher!!.doFinal(Base64.decode(textToDecrypt, Base64.NO_WRAP)), "UTF-8").toString()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return textDecrypted
    }

    fun String(doFinal: ByteArray?, s: String) = Unit

}


