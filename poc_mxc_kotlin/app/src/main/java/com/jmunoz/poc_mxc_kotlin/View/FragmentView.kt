package com.jmunoz.poc_mxc_kotlin.View

import android.app.Fragment
import android.app.FragmentManager
import android.content.Context
import android.view.View
import java.lang.ref.WeakReference

/**
 * Created by jmunoz on 04-07-17.
 */
open class FragmentView {

    var viewRef: WeakReference<View>? = null
    var context: Context? = null
    var fragment: Fragment? = null

    constructor(viewRef: WeakReference<View>?, context: Context?, fragment: Fragment?) {
        this.viewRef = viewRef
        this.context = context
        this.fragment = fragment
    }

//    fun getContext(): Any? = this.context

    fun getFragment(): View? = viewRef?.get()

    //
    fun getFragmentManager(): FragmentManager? = fragment?.fragmentManager

    constructor()


}