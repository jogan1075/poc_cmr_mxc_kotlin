package com.jmunoz.poc_mxc_kotlin.BS.Services

import android.content.Context
import android.util.Base64
import com.android.volley.*
import com.android.volley.toolbox.JsonObjectRequest
import com.jmunoz.poc_mxc_kotlin.BS.Utils.HttVerb
import com.jmunoz.poc_mxc_kotlin.BS.Utils.ResourceHandler
import com.jmunoz.poc_mxc_kotlin.Model.Callback
import org.json.JSONException
import org.json.JSONObject
import org.json.JSONTokener
import java.util.HashMap

/**
 * Created by jmunoz on 04-07-17.
 */
class ServiceInvokerAuth : ResourceHandler {
    var context: Context? = null
    override var call: Callback? = null

    constructor(callback: Callback?, context: Context?) : super(callback, context) {
        this.call = callback
        this.context = context
    }

    fun getTokenAccess(userEncrip: String) {
        getpDialog().show()
        val jsonObjReq = object : JsonObjectRequest(requestMethod,
                serviceURL, null,
                Response.Listener<JSONObject> { response ->
                    VolleyLog.d("EXAMPLE_POST", "Response: " + response)
                    try {
                        val json = JSONTokener(response.toString()).nextValue() as JSONObject
                        val test = json.get("access_token") as String
//                        callServices = BSSecurityDSCallServices(context, callBackInvokeServices)
//                        callServices.CallServices(test)
                        getCallBack()?.successCall(test)
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }

                    getpDialog().hide()
                }, Response.ErrorListener { error ->
            VolleyLog.d("EXAMPLE_POST", "Error: " + error.message)
            getCallBack()?.erroCall(error.message)
            getpDialog().hide()
        }) {
            override fun getBody(): ByteArray {
                return userEncrip.toByteArray()
            }

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()

                val credentials = "falabella-api-client:falabella1234"
                val auth = "Basic " + Base64.encodeToString(credentials.toByteArray(), Base64.NO_WRAP)
                headers.put("Content-Type", "application/x-www-form-urlencoded")
                headers.put("Authorization", auth)
                //                headers.put("Authorization", "Basic ZmFsYWJlbGxhLWFwaS1jbGllbnQ6ZmFsYWJlbGxhMTIzNA==");
                return headers
            }

            override fun getPriority(): Request.Priority {
                return priority
            }
        }

        jsonObjReq.retryPolicy = DefaultRetryPolicy(TIMEOUT_MILLISECONDS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)

        vollyRequest.getRequestQueue().getCache().clear()
        vollyRequest.getRequestQueue().add(jsonObjReq)
    }

    override fun getParamaters(json: String): String {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override val serviceURL: String
        get() = "http://192.168.10.196:8080/bankingserver-project-falabella/oauth/token"
    override val httpVerb: HttVerb
        get() = HttVerb.POST
}