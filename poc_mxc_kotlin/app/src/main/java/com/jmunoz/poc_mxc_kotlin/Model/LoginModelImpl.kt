package com.jmunoz.poc_mxc_kotlin.Model

import android.content.Context
import com.jmunoz.poc_mxc_kotlin.Facade.LoginFacade

/**
 * Created by jmunoz on 04-07-17.
 */
class LoginModelImpl : LoginModel {

    var context: Context? = null
    var facade: LoginFacade? = null

    constructor(context: Context?) {
        this.context = context
    }

    fun LoginUsuario(user: String, pass: String, callback: Callback?) {
        this.facade = LoginFacade(context, callback)
        this.facade?.authentication(user, pass)
    }
}