package com.jmunoz.poc_mxc_kotlin.BS

import android.content.Context
import com.jmunoz.poc_mxc_kotlin.BS.Services.ServiceInvokerAuth
import com.jmunoz.poc_mxc_kotlin.BS.Utils.RSAUtilSingleton
import com.jmunoz.poc_mxc_kotlin.DataSources.SecurityDSAuth
import com.jmunoz.poc_mxc_kotlin.Model.Callback
import java.io.UnsupportedEncodingException
import java.net.URLEncoder

/**
 * Created by jogan1075 on 04-07-17.
 */
class BSSecurityDSAuth : SecurityDSAuth {
    var context: Context? = null
    var rsaUtil: RSAUtilSingleton? = null
    var serviceInvokerAuth: ServiceInvokerAuth? = null

    constructor(context: Context?, callbacks: Callback?) {
        rsaUtil = RSAUtilSingleton()
        serviceInvokerAuth = ServiceInvokerAuth(callbacks, context)
    }


    override fun autentificacion(user: String, pass: String) {
        var request = ""
        rsaUtil?.loadPublicKey()
        try {
            request = "grant_type=password&username=" + URLEncoder.encode(rsaUtil?.encryptValue(user), "UTF-8") + "&password=" + URLEncoder.encode(rsaUtil?.encryptValue(pass), "UTF-8")
        } catch (e: UnsupportedEncodingException) {
            e.printStackTrace()
        }

        serviceInvokerAuth?.getTokenAccess(request)
    }
}