package com.jmunoz.poc_mxc_kotlin.UserCase

/**
 * Created by jogan1075 on 04-07-17.
 */
abstract class UseCase {

    internal abstract fun execute(user: String, pass: String)
}