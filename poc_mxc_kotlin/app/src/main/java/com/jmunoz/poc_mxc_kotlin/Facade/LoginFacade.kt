package com.jmunoz.poc_mxc_kotlin.Facade

import android.content.Context
import com.jmunoz.poc_mxc_kotlin.Model.Callback
import com.jmunoz.poc_mxc_kotlin.UserCase.LoginUserCase

/**
 * Created by jogan1075 on 04-07-17.
 */
class LoginFacade {

    var context: Context? = null
    var useCaseLogin: LoginUserCase? = null

    constructor(context: Context?, callbacks: Callback?) {
        this.context = context
        this.useCaseLogin =LoginUserCase(context,callbacks)
    }

    fun authentication(user: String, pass: String) {
        useCaseLogin?.execute(user, pass)
    }
}