package com.jmunoz.poc_mxc_kotlin.View.Utils

import com.jmunoz.poc_mxc_kotlin.Presenter.LoginPresenter
import com.squareup.otto.Bus

/**
 * Created by jogan1075 on 04-07-17.
 */
class BusProvider {

    private val BUS_INSTANCE = EventBus

    fun BusProvider() {}

    fun getInstance(): Bus {
        return BUS_INSTANCE
    }

    fun register(vararg objects: LoginPresenter?) {
        for (o in objects) {
            if (o != null) {
                BUS_INSTANCE.register(o)
            }
        }
    }

    fun unregister(vararg objects: LoginPresenter?) {
        for (o in objects) {
            if (o != null) {
                BUS_INSTANCE.unregister(o)
            }
        }
    }
}