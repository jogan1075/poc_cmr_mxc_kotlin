package com.jmunoz.poc_mxc_kotlin.View

import android.app.Fragment
import android.content.Context
import android.content.DialogInterface
import android.support.v7.app.AlertDialog
import android.view.View
import com.jmunoz.poc_mxc_kotlin.View.Entity.LoginData
import com.squareup.otto.Bus
import kotlinx.android.synthetic.main.fragment_main.view.*


/**
 * Created by jmunoz on 04-07-17.
 */
class LoginView : FragmentView {

    var bus: Bus? = null
    var view: View? = null
    var contexto :Context?=null

    constructor(fragment: Fragment, view: View?, context: Context, bus: Bus?) : super() {
        this.bus = bus
        this.view = view
this.contexto=context
        view?.btnAceptar?.setOnClickListener {
            this.bus?.post(OnActivityEvent(view?.EditUser?.text.toString(), view?.EditPass?.text.toString()))
        }
    }

    fun didResponse(message: String) {

        this.contexto?.let {
            AlertDialog.Builder(it)
                    .setTitle("Informacion de Servicio: ")
                    .setMessage(message)
                    .setCancelable(false)
                    .setPositiveButton("Aceptar") { dialog, which -> }.show()
        }
    }

    fun didResponseError(message: String) {

        contexto?.let {
            AlertDialog.Builder(it)
                    .setTitle("Error")
                    .setMessage(message)
                    .setCancelable(false)
                    .setPositiveButton("Aceptar") { dialog, which -> }.show()
        }
    }

    class OnActivityEvent(var user: String, var pass:String)
}